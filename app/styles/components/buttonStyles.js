/*================================================
 Button Styles

 Purpose: Base Button element Styling
 Hints: use mixins in styles/global and styles/theme files
 ================================================== */
// -- Style file merge dependency
import ObjectUtils from '../../utils/ObjectUtils.js';

// -- Global Import
import globalStylesIndex from './../global/index.js';
const { globalUtility, globalColors } = globalStylesIndex;

// -- Theme Import
import theme from '../theme/themeSetter.js';
const { activeTheme } = theme;




// -- Style Variables
const styleVars = {
    inputHeight: 40,
    borderRadius: globalStylesIndex.globalUtility.borderRadius,
    borderColor: activeTheme.borderColor,
    textColor: activeTheme.bg,
    bgColor: 'transparent',
    textPadding: 7,
    buttonMargin: 15
};


// -- Base Styles
export default{
    button:{
        height: 40,
        marginLeft: styleVars.buttonMargin,
        marginRight: styleVars.buttonMargin,
        borderRadius: styleVars.borderRadius,
        alignSelf: 'stretch',
        alignItems: 'center',
        justifyContent: 'center',
        overflow: 'hidden',
    },

    buttonFullWidth:{
        marginLeft: 0,
        marginRight: 0,
        borderRadius: 0
    },
    buttonInner:{
        flexDirection:'row',
        backgroundColor: 'gray',
        flex: 1,
        alignSelf: 'stretch',
        alignItems: 'center',
        justifyContent: 'center',
        borderWidth: 1,
    },
    buttonText:{
        color: '#fff',
        paddingLeft: styleVars.textPadding,
        paddingRight: styleVars.textPadding
    },
    buttonIcon:{
        color: '#fff',
        fontSize: 18,
    },
    title: {
        fontSize: 25,
        textAlign: 'left',
    },
    placeholderText:{
        color:activeTheme.textColorDark,
        fontSize: 25
    },
    buttonPrimary:{
        backgroundColor:activeTheme.buttonColor1,
        borderWidth:0
    },
    buttonPrimaryText:{
        color:activeTheme.buttonTextColor1
    },
    buttonSecondary:{
        backgroundColor:activeTheme.buttonColor2,
        borderWidth:0
    },
    buttonSecondaryText:{
        color:activeTheme.buttonTextColor2
    },
    buttonTertiary:{
        backgroundColor:activeTheme.buttonColor3,
        borderWidth:0,
    },
    buttonTertiaryText:{
        color:activeTheme.buttonTextColor3,
    },

}