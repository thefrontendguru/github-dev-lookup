/*================================================
 Theme Setter

 Purpose: to set the theme to be used within
 the './themes' directory
 ================================================== */

// -- Don't Touch
import ObjectUtils from '../../utils/ObjectUtils';
export default ObjectUtils.merge({}, theme)


// -- Set your theme here by changing the dir
import theme from './darkTheme.js';




