export default {
    tab: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
    tabStrip: {
        height: 50,
        flexDirection: 'row',
        paddingTop: 0,
        borderWidth: 1,
        borderBottomWidth: 0,
        borderLeftWidth: 0,
        borderRightWidth: 0,
        borderTopColor: 'rgba(0,0,0,0.05)',
    },
    icon: {
        flex: 1,
        height: 35,
        position: 'absolute',
        alignSelf: 'center',
        top: 5,
        paddingLeft: 15,
        paddingRight: 15
    },
    text:{
        bottom: 5,
        position: 'absolute',
        fontSize: 10,
        flex: 1,
        paddingLeft: 15,
        paddingRight: 15
    },
    tabUnderline: {
        position: 'absolute',
        bottom: 0,
        height: 3,
        backgroundColor: 'rgba(0,200,170,0.85)',
    },
    selectedTab: {
        color: 'rgba(0,200,170,0.85)'
    },
    unselectedTab: {
        color: 'rgba(0,0,0,0.1)'
    }
};