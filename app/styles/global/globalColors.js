/*================================================
 Global Colors

 Purpose: shortcut values for colors
 ================================================== */

export default{
    globalColors: {
        white: 'white',
        gray0: '#D8D8D8',
        gray1: 'gray',
        gray2: 'gray',
        gray3: 'gray',
        gray4: 'gray',
        black: 'black',
        blue: 'blue'
    }
}

