
import React, {
    AppRegistry,
    Component,
    StyleSheet,
    Text,
    NavigatorIOS,
    View
} from 'react-native';

import Main from './app/pages/Main.js'

class ReactNativeTut extends Component {
    render() {
        return (
            <NavigatorIOS
                style = {styles.container}
                initialRoute={{
                    component: Main,
                    title: 'GitHub NoteTaker'
                }}

            />
        );
    }
}

const styles = StyleSheet.create({
    container:{
        flex: 1,
        backgroundColor: '#111111'
    }
});

AppRegistry.registerComponent('ReactNativeTut', () => ReactNativeTut);
