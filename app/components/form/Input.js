import React, {TextInput, StyleSheet} from 'react-native';

import inputStyles from '../../styles/components/form/inputStyles.js';
const styles = StyleSheet.create(inputStyles);



export default class Input extends React.Component {


    render() {
        return (
            <TextInput
                style={styles.input}
                value={this.props.InputText}
                onChange={this.props.linkText}
                placeholder={this.props.placeholderText}
                />
        )
    }
}
