import React, {StyleSheet, View, ScrollView, TouchableHighlight, Text} from 'react-native';
import Badge from '../components/Badge';
import Separator from '../components/helpers/Separator';
import BrowserView from '../components/BrowserView'

var styles = StyleSheet.create({
    container: {
        flex: 1
    },
    rowContainer: {
        flexDirection: 'column',
        flex: 1,
        padding: 10
    },
    name: {
        color: '#48bbec',
        fontSize: 18,
        padding: 10
    },
    stars: {
        color: '#48bbec',
        fontSize: 14,
        paddingBottom: 5
    },
    description: {
        fontSize: 14,
        paddingBottom: 5
    },
});

export default class Repositories extends React.Component {

    openPage(url){
        this.props.navigator.push({
            component: BrowserView,
            title: '',
            passProps: {url}
        });
    }

    render() {
        var repos = this.props.repos;
        var list = repos.map((item, index) => {

            var desc = repos[index].description ? <Text style={styles.description}>{repos[index].description}</Text> : <View />;

            return (
                <View key={index}>
                    <View style={styles.rowContainer}>
                        <TouchableHighlight
                            onPress={this.openPage.bind(this, repos[index].html_url)}
                            underLayColor='transparent'>
                            <Text style={styles.name}>{repos[index].name}</Text>
                        </TouchableHighlight>
                        <Text style={styles.stars}>{repos[index].stargazers_count}</Text>
                        {desc}
                    </View>
                    <Separator />
                </View>
            )
        });
        return (
            <ScrollView style={styles.container}>
                <Badge userInfo={this.props.userInfo}/>
                {list}
            </ScrollView>
        )
    }
};

Repositories.propTypes ={
    userInfo: React.PropTypes.object.isRequired,
    repos: React.PropTypes.array.isRequired
};