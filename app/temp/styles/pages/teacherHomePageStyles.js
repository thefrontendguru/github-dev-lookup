/*================================================
 Teacher Home Page Styles
 ================================================== */

// -- Style file merge dependency
import ObjectUtils from '../../utils/ObjectUtils';

// -- Theme Import
import theme from '../themes/themeSetter.js';
const { activeTheme } = theme;



export default ObjectUtils.merge({
    container: {
        flex: 1,
    },

    tabView: {
        flex: 1,
        backgroundColor: activeTheme.bgColorLight
    }
});