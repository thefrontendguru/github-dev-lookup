import React, {View, WebView, StyleSheet} from 'react-native';

var styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#f6f6ef',
        flexDirection: 'column'
    },

});

export default class BrowserView extends React.Component {

    static propTypes = {
      url: React.PropTypes.string.isRequired
    };

    render() {
        return (
            <View style={styles.container}>
                <WebView url={this.props.url} />
            </View>
        )
    }
};

