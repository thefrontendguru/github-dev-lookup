let copyProperty = (des, key, obj) => {
    if (des[key] === undefined) {
        const val = obj[key];
        des[key] = typeof val === 'object' ? Object.assign({}, val) : val;
    } else if (typeof des[key] === 'object' || typeof obj[key] === 'object') {
        copyObject(des[key], obj[key]);
    } else {
        des[key] = obj[key];
    }
};

let copyObject = (des, src) => {
    Object.keys(src).map(key => copyProperty(des, key, src));
};

export default {
    merge(...args) {
        let result = {};
        args.map(obj => copyObject(result, obj));
        return result;
    },

    copy(dest, ...args) {
        args.map(obj => copyObject(dest, obj));
        return dest;
    }
}