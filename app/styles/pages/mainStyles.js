/*================================================
 Main Styles

 Purpose: Component Styles
 ================================================== */
// -- Style file merge dependency
import ObjectUtils from '../../utils/ObjectUtils.js';

// -- Global Import
import globalStylesIndex from './../global/index.js';
const { globalUtility, globalColors } = globalStylesIndex;

// -- Theme Import
import theme from '../theme/themeSetter.js';
const { activeTheme } = theme;


// -- Style Variables
const styleVars = {
    padding: 30
};


// -- Styles
export default{

    mainContainer:{
        flex: 1,
        //padding: styleVars.padding,
        marginTop: 65,
        flexDirection: 'column',
        justifyContent: 'center',
        backgroundColor: activeTheme.bgColorDark,
        alignItems: 'center'
    },

    inputTitle:{
        marginBottom: 20,
        fontSize: 25,
        textAlign: 'center',
        color: globalColors.black
    },

    searchButton:{
        marginTop:15
    },

    buttonText:{
        color: '#fff'
    }
}