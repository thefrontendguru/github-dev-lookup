/*================================================
 Global Utility

 Purpose: shortcut values for utilities
 ================================================== */

export default{
    globalUtility: {
        borderRadius: 5,
        borderWidth: 1,

    }
}