/*================================================
 Global Index

 Purpose: to set and use global vars
 ================================================== */

// -- Don't Touch
import ObjectUtils from '../../utils/ObjectUtils';
export default ObjectUtils.merge({}, globalUtility, globalColors)

// -- Imports
import globalUtility from './globalUtility.js';
import globalColors from './globalColors.js';