import React, {StyleSheet, View} from 'react-native';

let styles = StyleSheet.create({
    separator:{
        flex: 1,
        height: 1,
        backgroundColor: 'grey',
        marginLeft: 15
    }
});

export default class Separator extends React.Component{
    render(){
        return(
            <View style={styles.separator} />
        )
    }
}