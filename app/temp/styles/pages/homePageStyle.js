export default {
    drawerStyle: {shadowColor: '#000000', shadowOpacity: 0.2, shadowRadius: 3},
    headerStyle: {backgroundColor: 'rgba(255,100,0,.65)', borderBottomWidth: 1, borderBottomColor: 'rgba(0,0,0,0.15)'}
};