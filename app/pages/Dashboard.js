import React, {View, Text, StyleSheet, Image, TouchableHighlight} from 'react-native';
import Button from './../components/Button.js';

import dashboardStyles from '../styles/pages/dashboardStyles.js';
const styles = StyleSheet.create(dashboardStyles);

import api from '../utils/api.js'

import Profile from './Profile.js';
import Repositories from './Repositories.js';
import Notes from './Notes.js'


export default class Dashboard extends React.Component {

    render() {
        return (
            <View style={styles.container}>
                <Image style={styles.image} source={{uri: this.props.userInfo.avatar_url}} />
                <Button
                    contextStyle={styles.button}
                    bgStyle={styles.dashboardButton1}
                    fullWidth="true"
                    onButtonPress={this.goToProfile.bind(this)}>
                    View Profile</Button>
                <Button
                    contextStyle={styles.button}
                    bgStyle={styles.dashboardButton2}
                    fullWidth="true"
                    onButtonPress={this.goToRepos.bind(this)}>
                    Repositories</Button>
                <Button
                    contextStyle={styles.button}
                    bgStyle={styles.dashboardButton3}
                    fullWidth="true"
                    onButtonPress={this.goToNotes.bind(this)}>
                    Notes</Button>

            </View>
        )
    }

    goToProfile(){
        console.log('Go To Profile button Pressed');
        this.props.navigator.push({
            title: 'Profile Page',
            component: Profile,
            passProps: {userInfo: this.props.userInfo}
        });
    }

    goToRepos(){
        api.getRepos(this.props.userInfo.login).
            then((res) =>{
                this.props.navigator.push({
                    title: 'Repo Page',
                    component: Repositories,
                    passProps: {userInfo: this.props.userInfo, repos: res}
                })
            });
        console.log('Go To Repos button Pressed');

    }

    goToNotes(){
        console.log('Go To Notes button Pressed')
        api.getNotes(this.props.userInfo.login).
            then((res) =>{
                res = res || {};
                this.props.navigator.push({
                    title: 'Notes',
                    component: Notes,
                    passProps: {userInfo: this.props.userInfo, notes: res}
                })
            });
    }
};