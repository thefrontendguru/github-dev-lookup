import React, {Text, View, StyleSheet, ScrollView} from 'react-native';

import Badge from '../components/Badge.js';
import Separator from '../components/helpers/Separator.js'

var styles = StyleSheet.create({
    container: {
        flex: 1
    },
    buttonText: {
        fontSize: 18,
        color: 'white',
        alignSelf: 'center'
    },
    rowContainer: {
        padding: 10
    },
    rowTitle: {
        color: '#48BBEC',
        fontSize: 16
    },
    rowContent: {
        fontSize: 19
    }
});

export default class Profile extends React.Component {
    render() {
        let {userInfo} = this.props;
        let topicArr = ['company', 'login','location', 'followers', 'following', 'email', 'bio'];

        let list = topicArr.map((item, index)=>{
           if(!userInfo[item]){
               return <View key={index} />
           } else{
               return(
                    <View key={index}>
                        <View style={styles.rowContainer}>
                            <Text style={styles.rowTitle}> {item}</Text>
                            <Text style={styles.rowContent}> {userInfo[item]}</Text>
                        </View>
                        <Separator />
                    </View>
               )
           }
        });

        return (
            <ScrollView style={styles.container}>
                <Badge userInfo={userInfo} />
                {list}
            </ScrollView>
        );
    }
}