/*================================================
 Login Styles
 ================================================== */


import { Dimensions } from 'react-native';
const { width, height } = Dimensions.get('window');


// -- Style file merge dependency
import ObjectUtils from '../../utils/ObjectUtils';

// -- Global Import
import globalStylesIndex from './../global/index.js';
const { globalUtility, globalColors } = globalStylesIndex;

// -- Theme Import
import theme from '../themes/themeSetter.js';
const { activeTheme } = theme;



export default ObjectUtils.merge({
    container: {
        flexDirection: 'column',
        flex: 1,
        backgroundColor: 'transparent'
    },
    bg: {
        position: 'absolute',
        left: 0,
        top: 0,
        width: width,
        height: height
    },
    header: {
        justifyContent: 'center',
        alignItems: 'center',
        flex: .5,
        backgroundColor: 'transparent'
    },
    mark: {
        width: 150,
        height: 150
    },
    signin: {
        backgroundColor: activeTheme.heroColor1,
        padding: 20,
        alignItems: 'center',
        borderRadius: globalUtility.borderRadius,
        marginLeft: 20,
        marginRight: 20
    },
    signup: {
        justifyContent: 'center',
        alignItems: 'center',
        flex: .15
    },
    inputs: {
        marginTop: 10,
        marginBottom: 10,
        flex: .25
    },
    inputPassword: {
        marginLeft: 15,
        width: 20,
        height: 21
    },
    inputUsername: {
        marginLeft: 15,
        width: 20,
        height: 20
    },
    inputContainer: {
        padding: 10,
        borderWidth: 1,
        borderBottomColor: '#CCC',
        borderColor: 'transparent'
    },
    input: {
        position: 'absolute',
        left: 61,
        top: 12,
        right: 0,
        height: 20,
        fontSize: 14
    },
    forgotContainer: {
        alignItems: 'flex-end',
        padding: 15,
    },
    greyFont: {
        color: globalColors.gray0
    },
    whiteFont: {
        color: globalColors.white
    }
});