

export default{

    activeTheme: {

        // -- High Level
        heroColor1: '#FF3366',
        heroColor2: '#000000',
        heroColor3: '#000000',
        successColor: '',
        errorColor: '',
        warningColor: '',


        // -- Background Color
        bgColorLight: '#ffffff',
        bgColorDark: '#48bbec',


        // -- Text Color
        textColorLight: '#ffffff',
        textColorDark: '#000000',
        textColorLink: '#789678',


        // -- Border Color
        borderColor: '#e6e6e6',


        // -- Button Color
        buttonColor1: 'darkblue',
        buttonTextColor1: 'white',
        buttonColor1Selected: '#000080',

        buttonColor2: 'gray',
        buttonTextColor2: '#000000',
        buttonColor2Selected: '#000000',

        buttonColor3: '#000000',
        buttonTextColor3: '#000000',
        buttonColor3Selected: '#000000'

    }

}

