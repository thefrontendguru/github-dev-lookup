/*================================================
 Form: Input Styles

 Purpose: Base Form element Styling
 Hints: use mixins in styles/global and styles/theme files
 ================================================== */
// -- Style file merge dependency
import ObjectUtils from '../../../utils/ObjectUtils.js';

// -- Global Import
import globalStylesIndex from './../../global/index.js';
const { globalUtility, globalColors } = globalStylesIndex;

// -- Theme Import
import theme from '../../theme/themeSetter.js';
const { activeTheme } = theme;





// -- Style Variables
const styleVars = {
    inputHeight: 40,
    borderRadius: 5,
    borderColor: activeTheme.borderColor,
    textColor: activeTheme.bg,
    bgColor: 'transparent'
};


// -- Base Styles
export default{
    input: {
        height: styleVars.inputHeight,
        backgroundColor: styleVars.bgColor,
        borderWidth: 1,
        padding: 10,
        margin: 15,
        marginBottom: 0,
        borderRadius: globalUtility.borderRadius,
        borderColor: styleVars.borderColor,
        color: styleVars.textColor,
    },
    title: {
        fontSize: 25,
        textAlign: 'left',
    },
    placeholderText:{
        color:activeTheme.textColorDark,
        fontSize: 25
    }

}