export default {
    container: {flex: 1, backgroundColor: 'rgba(120,20,90,0.75)', paddingTop: 50},
    listItem: {
        padding: 10,
        borderWidth:1,
        borderTopWidth: 1,
        borderBottomWidth: 1,
        borderLeftWidth: 0,
        borderRightWidth: 0,
        borderColor: 'rgba(255,255,255,0.2)',
        borderTopColor: 'rgba(255,255,255,0.2)',
        borderBottomColor: 'rgba(255,255,255,0.2)',
        color: 'rgba(255,255,255,0.5)'
    }
};