/*================================================
 Page: Dashboard Styles

 Purpose: Base Form element Styling
 Hints: use mixins in styles/global and styles/theme files
 ================================================== */
// -- Style file merge dependency
import ObjectUtils from '../../utils/ObjectUtils.js';

// -- Global Import
import globalStylesIndex from '../global/index.js';
const { globalUtility, globalColors } = globalStylesIndex;

// -- Theme Import
import theme from '../theme/themeSetter.js';
const { activeTheme } = theme;


// -- Style Variables
const styleVars = {

};


// -- Base Styles
export default{
    container:{
        marginTop: 65,
        flex: 1,
    },
    image: {
        height: 350,
    },
    button:{
        flex: 1
    },
    dashboardButton1: {
        backgroundColor: 'green'
    },
    dashboardButton2: {
        backgroundColor: 'purple'
    },
    dashboardButton3: {
        backgroundColor: 'blue'
    }

}