import React, {View, Text,StyleSheet, TextInput, ActivityIndicatorIOS} from 'react-native';
import api from '../utils/api.js';
import Dashboard from './Dashboard.js';
import Input from './../components/form/Input.js';
import Button from './../components/Button.js';

import mainStyles from '../styles/pages/mainStyles.js';
const styles = StyleSheet.create(mainStyles);




export default class Main extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            username: '',
            isLoading: false,
            error: false
        }
    }

    static defaultProps = {
        namePlaceholder: 'Enter Github Username...',
        searchButtonText: 'Search It Brah'
    };

    render() {

        var showErr = (
            this.state.error ? <Text>{this.state.error}</Text> : <View></View>
        );

        return (
            <View style={styles.mainContainer}>
                <Text style={styles.inputTitle}>Search For a Github User</Text>
                <Input
                    InputText={this.state.username}
                    linkText={this.handleChange.bind(this)}
                    placeholderText={this.props.namePlaceholder}
                    />
                <Button
                    isPrimary="true"
                    iconName='ios-search'
                    iconPos='left'
                    contextStyle={styles.searchButton}
                    onButtonPress={this.handleSubmit.bind(this)}>
                    {this.props.searchButtonText}
                </Button>
                <ActivityIndicatorIOS
                    animating={this.state.isLoading}
                    color='#111111'
                    size='large'
                    ></ActivityIndicatorIOS>
                {showErr}
            </View>
        );

    }


    handleChange(e) {
        this.setState({
            username: e.nativeEvent.text
        })
    }

    handleSubmit() {
        //Update Activity Indicator
        this.setState({
            isLoading: true
        });

        console.log("this is the username: " + this.state.username);


        //fetch data from gitHub
        api.getBio(this.state.username)
            .then((res) => {
                if (res.message === "Not Found") {
                    this.setState({
                        error: 'User Not Found',
                        isLoading: false
                    });
                    console.log(this.state.error)
                } else {
                    this.props.navigator.push({
                        title: res.name || 'Select an Option',
                        component: Dashboard,
                        passProps: {userInfo: res}
                    });
                    this.setState({
                        username: '',
                        isLoading: false,
                        error: false
                    });
                }
            });

        //reroute to the next passing that github info
        console.log('SUBMIT ' + this.state.username);
    }
}