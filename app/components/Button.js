import React, {TouchableHighlight, Text, View, StyleSheet} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';

import buttonStyles from '../styles/components/buttonStyles.js';
const styles = StyleSheet.create(buttonStyles);

export default class Button extends React.Component {

    static defaultProps = {
        fullWidth: false,
        isPrimary: false,
        isSecondary: false,
        isTertiary: false,
        iconName: null,         // get icon names here >> http://ionicons.com/
        iconPos: null,          // either left or right
        contextStyle: '',        //any additional styles
        bgStyle:'',             // for background colors
        labelStyle:''           // for text colors
    };

    static propTypes ={
        name: React.PropTypes.string
    };

    render() {

        const {iconPos,
            iconName,
            isPrimary,
            isSecondary,
            isTertiary,
            fullWidth,
            contextStyle,
            bgStyle,
            labelStyle,
            children} = this.props;


        let buttonType = ()=>{
            if (isPrimary){
                return (styles.buttonPrimary)
            }else if(isSecondary){
                return (styles.buttonSecondary)
            }else if(isTertiary){
                return (styles.buttonTertiary)
            }else{
                return null
            }
        };

        let buttonTypeText = ()=>{
            if (isPrimary){
                return (styles.buttonPrimaryText)
            }else if(isSecondary){
                return (styles.buttonSecondaryText)
            }else if(isTertiary){
                return (styles.buttonTertiaryText)
            }else{
                return null
            }
        };
        let baseStyles = (fullWidth ? [styles.button, styles.buttonFullWidth] : styles.button);

        let buttonInnerStyles = [styles.buttonInner, (buttonType()), bgStyle];
        let buttonTextStyles = [styles.buttonText, (buttonTypeText()), labelStyle];
        let buttonIconStyles = [styles.buttonIcon, (buttonTypeText()), labelStyle];

        let iconLeft = (
            <View style={buttonInnerStyles}>
                <Icon name={this.props.iconName} style={buttonIconStyles}/>
                <Text style={buttonTextStyles}>{children}</Text>
            </View>
        );

        let iconRight = (
            <View style={buttonInnerStyles}>
                <Text style={buttonTextStyles}>{children}</Text>
                <Icon name={this.props.iconName} style={buttonIconStyles}/>
            </View>
        );

        let noIcon = (
            <View style={buttonInnerStyles}>
                <Text style={buttonTextStyles}>{children}</Text>
            </View>
        );

        let iconConfig = () => {
            if (iconName == null) {
                return noIcon
            } else if (iconPos == 'right') {
                return iconRight
            } else if (iconPos == 'left') {
                return iconLeft
            } else {
                return noIcon
            }
        };



        return (
            <TouchableHighlight
                style={[baseStyles, contextStyle]}
                onPress={this.props.onButtonPress}
                activeOpacity={0.8}
                underlayColor={'black'}>
                {iconConfig()}
            </TouchableHighlight>
        )
    }
}



